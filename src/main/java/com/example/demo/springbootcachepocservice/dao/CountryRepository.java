package com.example.demo.springbootcachepocservice.dao;

import com.example.demo.springbootcachepocservice.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Country repository
 */
public interface CountryRepository extends JpaRepository<Country, Long> {

    /**
     * Find country by name
     * @param name {@link String} country name
     * @return {@link Country}
     */
    Country findByName(String name);

    /**
     * Find country by alpha2Code
     * @param alpha2Code {@link String} alpha2Code
     * @return
     */
    Country findByAlpha2Code(String alpha2Code);
}
