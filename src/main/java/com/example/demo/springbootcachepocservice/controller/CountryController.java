package com.example.demo.springbootcachepocservice.controller;

import com.example.demo.springbootcachepocservice.services.CountryService;
import org.springframework.cache.CacheManager;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Country Controller
 */
@RestController
public class CountryController {

    private CountryService countryService;

    private CacheManager cacheManager;

    /**
     * Country controller constructor
     *
     * @param cacheManager   {@link CacheManager} cache manager
     * @param countryService {@link CountryService} country service
     */
    public CountryController(final CacheManager cacheManager, final CountryService countryService) {
        this.cacheManager = cacheManager;
        this.countryService = countryService;
    }

    /**
     * Find country by name
     *
     * @param name {@link String} country name
     * @return {@link String}
     */
    @GetMapping(value = "/country/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getByName(final @PathVariable String name) {
        return ResponseEntity.ok(countryService.findByName(name));
    }
}
