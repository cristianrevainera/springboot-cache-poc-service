package com.example.demo.springbootcachepocservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application main class
  */
@SpringBootApplication
public class SpringbootCachePocServiceApplication {
/**
* Application start point
* @param args command line parameters
*/
public static void main(final String[] args) {
SpringApplication.run(SpringbootCachePocServiceApplication.class, args);
}
}
