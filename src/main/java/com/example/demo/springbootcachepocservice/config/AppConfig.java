package com.example.demo.springbootcachepocservice.config;

import com.example.demo.springbootcachepocservice.entities.Country;
import com.example.demo.springbootcachepocservice.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ConcurrentHashMap;


/**
 * Caching config
 */
@Configuration
@EnableCaching
@EnableScheduling
public class AppConfig {

    private static final String FIND_BY_NAME = "findByName";

    @Autowired
    private CountryService countryService;

    /**
     * Makes RestTeamplateBuilder bean
     * @param builder @link RestTemplateBuilder} rest template builder
     * @return Resttemplate
     */
    @Bean
    public RestTemplate restTemplate(final RestTemplateBuilder builder) {
        return builder.build();
    }

    /**
     * Makes a bean of type ConcurrentMapCacheManager
     * @return
     */
    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(FIND_BY_NAME);
    }

    /**
     * Scheduler
     */
    @Scheduled(fixedRate = 30000)
    public void saveCountryCache() {
        ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) cacheManager().getCache(FIND_BY_NAME).getNativeCache();

        for (Object o : concurrentHashMap.values()) {
            Country country  = (Country) o;
            Country chachedCountry = countryService.findByAlpha2Code(country.getAlpha2Code());
            if (chachedCountry == null || (country.equals(chachedCountry) && country.hashCode() != chachedCountry.hashCode())) {
                countryService.save(country);
            }
        }
    }

}
