package com.example.demo.springbootcachepocservice.services;

import com.example.demo.springbootcachepocservice.entities.Country;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;


/**
 * Parameterized Type Reference implementation
 */
public class ParameterizedTypeReferenceCountries extends ParameterizedTypeReference<List<Country>> {
}
