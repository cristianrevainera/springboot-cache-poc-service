package com.example.demo.springbootcachepocservice.services;

import com.example.demo.springbootcachepocservice.dao.CountryRepository;
import com.example.demo.springbootcachepocservice.entities.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


/**
 * Country Service
 */
@Service
public class CountryServiceImpl implements CountryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountryServiceImpl.class);

    private CountryRepository countryRepository;
    private RestTemplate restTemplate;

    /**
     * Rest template contructor
     * @param restTemplate {@link RestTemplate} rest template
     * @param countryRepository {@link CountryService} country service
     */
    @Autowired
    public CountryServiceImpl(final RestTemplate restTemplate, final CountryRepository countryRepository) {
        this.restTemplate = restTemplate;
        this.countryRepository = countryRepository;
    }

    /**
     * Retrieves a country
     * @param name {@link String} country name
     * @return {@link Country} country
     */
    @Cacheable("findByName")
    @Override
    public Country findByName(final String name) {

        LOGGER.info("CountryServiceImpl#findByName");

        String url = "https://restcountries.eu/rest/v2/name/" + name;
        ResponseEntity<List<Country>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReferenceCountries());

        List<Country> countries = (List<Country>) response.getBody();
        return countries.get(0);
    }

    @Override
    public Country save(final Country country) {
        return countryRepository.save(country);
    }


    @Override
    public Country findByAlpha2Code(final String alpha2Code) {
        return countryRepository.findByAlpha2Code(alpha2Code);
    }
}
