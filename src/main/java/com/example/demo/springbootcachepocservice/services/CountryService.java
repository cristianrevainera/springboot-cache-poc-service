package com.example.demo.springbootcachepocservice.services;

import com.example.demo.springbootcachepocservice.entities.Country;

/**
 * Country Service
 */
public interface CountryService {
    /**
     * Retrieves a country
     * @param name {@link String}
     * @return
     */
    Country findByName(String name);


    /**
     * Find by alpha2Code
     * @param alpha2Code {@link String}
     * @return
     */
    Country findByAlpha2Code(String alpha2Code);


    /**
     * Save country
     * @param country {@link Country}
     * @return
     */
    Country save(Country country);
}
