package com.example.demo.springbootcachepocservice.exceptions;

/**
 * Enum con los errores (una cuestion de prolijidad.
 */
public enum ErrorEnum {
    DATABASE_ERROR("General data base error."),
    SERVICE_ERROR("General service error."),
    REGISTRY_NOT_FOUND_ERROR("Registry not found");


    private String msg;

    /**
     *
     * @param msg {@link String}
     */
    ErrorEnum(final String msg) {
        this.msg = msg;
    }

    /**
     * Gets msg.
     *
     * @return Value of msg.
     */
    public String getMsg() {
        return msg;
    }


}
