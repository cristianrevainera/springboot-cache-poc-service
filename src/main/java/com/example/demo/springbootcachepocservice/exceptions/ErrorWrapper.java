package com.example.demo.springbootcachepocservice.exceptions;

/**
 * Una Wrapper para los errores pasa de String a Object.
 */
public class ErrorWrapper {
    private String message;


    /**
     *
     * @param message {@link String}
     */
    public ErrorWrapper(final String message) {
        this.message = message;
    }


    /**
     * Gets message.
     *
     * @return message {@link String} Value of message.
     */
    public String getMessage() {
        return message;
    }
}
